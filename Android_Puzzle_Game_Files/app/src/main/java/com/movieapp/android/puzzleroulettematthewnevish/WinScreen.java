package com.movieapp.android.puzzleroulettematthewnevish;

        import android.os.Bundle;
        import android.os.Handler;
        import android.support.v7.app.AppCompatActivity;

/**
 * Created by Mat on 12/15/2016.
 */

public class WinScreen extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.win_screen);
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };
        handler.postDelayed(r, 3000);
    }
}
