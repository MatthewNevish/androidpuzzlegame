package com.movieapp.android.puzzleroulettematthewnevish;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements OnClickListener  {

    ImageButton imageButton1;
    ImageButton imageButton2;
    ImageButton imageButton3;
    ImageButton imageButton4;
    ImageButton imageButton5;
    ImageButton imageButton6;
    ImageButton imageButton7;
    ImageButton imageButton8;
    ImageButton imageButton9;

    //List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

    int randomInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Random rand = new Random();
        randomInt = rand.nextInt(9) + 1;

        imageButton1 = (ImageButton) findViewById(R.id.gridButton1);
        imageButton1.setOnClickListener(this);
        imageButton2 = (ImageButton) findViewById(R.id.gridButton2);
        imageButton2.setOnClickListener(this);
        imageButton3 = (ImageButton) findViewById(R.id.gridButton3);
        imageButton3.setOnClickListener(this);
        imageButton4 = (ImageButton) findViewById(R.id.gridButton4);
        imageButton4.setOnClickListener(this);
        imageButton5 = (ImageButton) findViewById(R.id.gridButton5);
        imageButton5.setOnClickListener(this);
        imageButton6 = (ImageButton) findViewById(R.id.gridButton6);
        imageButton6.setOnClickListener(this);
        imageButton7 = (ImageButton) findViewById(R.id.gridButton7);
        imageButton7.setOnClickListener(this);
        imageButton8 = (ImageButton) findViewById(R.id.gridButton8);
        imageButton8.setOnClickListener(this);
        imageButton9 = (ImageButton) findViewById(R.id.gridButton9);
        imageButton9.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.gridButton1 :
                if(randomInt == 1){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton1.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton1.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton2 :
                if(randomInt == 2){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton2.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton2.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton3 :
                if(randomInt == 3){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton3.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton3.setImageResource(R.drawable.wrong);

                }
                break;
            case R.id.gridButton4 :
                if(randomInt == 4){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton4.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton4.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton5 :
                if(randomInt == 5){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton5.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton5.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton6 :
                if(randomInt == 6){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton6.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton6.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton7 :
                if(randomInt == 7){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton7.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton7.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton8 :
                if(randomInt == 8){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton8.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton8.setImageResource(R.drawable.wrong);
                }
                break;
            case R.id.gridButton9 :
                if(randomInt == 9){
                    Toast.makeText(MainActivity.this,"You found the dragon! Congratulations!", Toast.LENGTH_LONG).show();
                    imageButton9.setImageResource(R.drawable.dragon);

                    Intent intent = new Intent(MainActivity.this, WinScreen.class);
                    finish();
                    MainActivity.this.startActivity(intent);
                }
                else{
                    imageButton9.setImageResource(R.drawable.wrong);
                }
                break;
        }
    }
}
